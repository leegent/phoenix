from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from reportlab.lib import colors
from reportlab.lib.pagesizes import landscape, A4, portrait
from reportlab.lib.units import cm
from reportlab.platypus import BaseDocTemplate, PageTemplate, Frame, Table, TableStyle

from phoenix.models import Session


class AttendancePage(PageTemplate):
    def __init__(self, page_size, title, location, date, *args, **kwargs):
        self.page_size = page_size
        self.title = title
        self.date = date
        self.location = location
        self.page_height = self.page_size[1]
        self.page_width = self.page_size[0]
        self.log_frame = Frame(1 * cm, 1.5 * cm, self.page_width - (2 * cm), self.page_height - (4.5 * cm), id='TableFrame', showBoundary=0)
        super(AttendancePage, self).__init__(id='AttendancePage', frames=[self.log_frame])

    def beforeDrawPage(self, canvas, doc):
        canvas.saveState()
        canvas.setFont('Times-Bold', 14)
        canvas.drawString(1*cm, self.page_height - (1.5*cm), self.title)
        canvas.drawRightString(self.page_width - (1*cm), self.page_height - (2*cm), self.date.ctime())
        canvas.drawRightString(self.page_width - (1*cm), self.page_height - (2.5*cm), self.location)

        #canvas.drawString(1*cm, self.page_height - (1.5*cm), self.location)
        #canvas.drawCentredString(self.page_width / 2.0, self.page_height - (1.5*cm), self.title)
        #canvas.drawRightString(self.page_width - (1*cm), self.page_height - (1.5*cm), self.date.ctime())

        canvas.restoreState()

        canvas.saveState()
        canvas.setFont('Times-Roman', 9)
        #canvas.drawString(1 * cm, (1 * cm), 'Not for Official Use')
        canvas.drawCentredString(self.page_width / 2.0, (1 * cm), "Page {}".format(doc.page))
        #canvas.drawRightString(self.page_width - (1 * cm), (1 * cm), "Not for Official Use")
        canvas.restoreState()


def make_pdf(stream, session: Session) -> None:
    ps = portrait(A4)
    doc = BaseDocTemplate(
        pageTemplates=[
            AttendancePage(ps, f'{settings.PHOENIX_NAME} Session Attendance Summary', session.place, session.time)
        ],
        filename=stream,
        pagesize=ps,
        title= f'{settings.PHOENIX_NAME} - {session.time} at {session.place}',
        author='Nottingham Hellfire Harlots Roller Derby')

    data = [
        [
            'Person',
            'Temperature (optional)',
            'Temp Check Passed',
            'Contact Number',

        ]
    ]
    for record in session.sessionattendance_set.all():
        row = [
            record.member.legal_name,
            record.temperature or 'N/A',
            record.test_pass,
            record.member.contact_number
        ]
        data.append(row)

    t = Table(data)
    t.setStyle(TableStyle([('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
                           ('ALIGN', (0, 0), (-1, 0), 'CENTRE'),
                           ('ALIGHT', (0, 0), (-1, 0), 'CENTRE'),
                           ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                           ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                           ]))

    doc.build([t])


@login_required
def print_session(request, session_id:int):
    if not request.user.is_superuser:
        return HttpResponseForbidden('You need to be an admin to do this!')

    s: Session = get_object_or_404(Session, pk=session_id)
    doc_title = f'{settings.PHOENIX_NAME} - {s.time} at {s.place}'

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = f'attachment; filename="{doc_title}.pdf"'

    make_pdf(response, s)

    return response

