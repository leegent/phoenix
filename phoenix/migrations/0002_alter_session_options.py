# Generated by Django 3.2.3 on 2021-05-16 12:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('phoenix', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='session',
            options={'ordering': ['-time']},
        ),
    ]
