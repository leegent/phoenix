from datetime import timedelta

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils import timezone

from phoenix.models import Session, Member, SessionAttendance, Bubble


@login_required
def index(request):
    now = timezone.now()
    # future is everything beyond now (but let's say yeseterday)
    margin = timedelta(hours=settings.SEND_MAIL_DEADLINE_HOURS)
    lower_horizon = now - margin
    sessions = Session.objects.filter(time__gte=lower_horizon).order_by('time')
    previous_sessions = Session.objects.filter(time__lt=lower_horizon)
    ctx = dict(
        sessions=sessions,
        past_sessions=previous_sessions
    )
    return render(request, 'phoenix/sessions.html', ctx)


@login_required
def session(request, session_id: int):
    s: Session = get_object_or_404(Session, pk=session_id)
    ctx = dict(
        session=s
    )
    return render(request, 'phoenix/session.html', ctx)


@login_required
def record_attendee(request, session_id: int):
    s: Session = get_object_or_404(Session, pk=session_id)
    ctx = dict(
        session=s,
        members=s.bubble.member_set.exclude(pk__in=s.attendees.all())
    )
    if request.method == 'POST':
        m: Member = get_object_or_404(Member, pk=request.POST['member'])
        t = request.POST['temperature'] or None
        if t:
            t = int(t)
        test_passed = 'test_passed' in request.POST
        sa, new = SessionAttendance.objects.update_or_create(member=m, session=s, temperature=t, test_pass=test_passed)
        return HttpResponseRedirect(reverse('session', args=[session_id]))

    return render(request, 'phoenix/record.html', ctx)


@login_required
def delete_attendee(request, attendee_id: int):
    sa = get_object_or_404(SessionAttendance, pk=attendee_id)
    session_id = sa.session_id
    sa.delete()
    return HttpResponseRedirect(reverse('session', args=[session_id]))


def format_ical_date_string(dt):
    return dt.strftime('%Y%m%dT%H%M%SZ')


EVENT_DURATION = timezone.timedelta(0, 0, 0, 0, 0, 2)


def ical(request, bubble: str):
    b = get_object_or_404(Bubble, name=bubble)
    resp = HttpResponse(content_type='text/calendar')

    resp.write('BEGIN:VCALENDAR\n')
    resp.write('VERSION:2.0\n')
    resp.write('PRODID:-//RDEC//NONSGML v1.0//EN\n')
    now = timezone.now()

    then = now - timedelta(days=1)

    events = Session.objects.filter(bubble=b).filter(time__gt=then).order_by('time')
    for e in events:

        end = e.time + EVENT_DURATION

        resp.write('BEGIN:VEVENT\n')
        resp.write('UID:{}@{}\n'.format(e.pk, request.get_host()))
        resp.write('DTSTAMP:{}\n'.format(format_ical_date_string(e.last_modified)))
        resp.write('DTSTART:{}\n'.format(format_ical_date_string(e.time)))
        resp.write('DTEND:{}\n'.format(format_ical_date_string(end)))
        resp.write('SUMMARY:{} ({})\n'.format(f'{settings.PHOENIX_NAME} Bubble {bubble} Session', e.place))

        resp.write('END:VEVENT\n')

    resp.write('END:VCALENDAR\n')
    return resp
