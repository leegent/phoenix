from django.conf import settings
from django.contrib import admin

# Register your models here.
from django.contrib.admin import AdminSite, ModelAdmin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth.models import Group

from phoenix.models import Bubble, Session, Member, SessionAttendance


class PhoenixAdminSite(AdminSite):
    site_header = f'{settings.PHOENIX_NAME} PHOENIX Administration'
    site_title = f'{settings.PHOENIX_NAME} PHOENIX Administration'
    index_title = f'{settings.PHOENIX_NAME} PHOENIX Administration'


class MemberAdmin(ModelAdmin):
    list_display = (
        'legal_name',
        'bubble',
        'locale',
        'contact_number',
    )
    list_filter = (
        'bubble',
        'locale',
    )
    search_fields = (
        'legal_name',
        'locale',
    )
    ordering = ('legal_name', 'bubble',)


class SessionAdmin(ModelAdmin):
    list_filter = (
        'bubble',
        'place',
    )


admin_site = PhoenixAdminSite(name='phoenix_admin')
admin_site.register(get_user_model(), UserAdmin)
admin_site.register(Group, GroupAdmin)
admin_site.register(Bubble)
admin_site.register(Session, SessionAdmin)
admin_site.register(Member, MemberAdmin)
admin_site.register(SessionAttendance)
