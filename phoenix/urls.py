from django.urls import path
from . import views, print_views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:session_id>/', views.session, name='session'),
    path('<int:session_id>/print', print_views.print_session, name='print_session'),
    path('<int:session_id>/record/', views.record_attendee, name='record_attendee'),
    path('attendance/<int:attendee_id>/delete/', views.delete_attendee, name='delete_attendee'),
    path('bubbles/<bubble>/ical.ics', views.ical, name='ical')
]
