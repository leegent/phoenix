import tempfile
from datetime import timedelta

from django.conf import settings
from django.core.mail import EmailMessage
import dropbox
from django.utils import timezone

from harlotsphoenix.celery import app



def send_reminders():
    from phoenix.models import Session

    send_reminder_hours = settings.SEND_MAIL_DEADLINE_HOURS - settings.SEND_MAIL_DEADLINE_REMINDER_HOURS
    now = timezone.now()

    send_reminders_horizon = now - timedelta(hours=send_reminder_hours)
    send_reminder_sessions = Session.objects.filter(stats_uploaded=False, warning_sent=False,
                                                    time__lte=send_reminders_horizon)
    if send_reminder_sessions:
        for send_reminder_session in send_reminder_sessions:
            email = EmailMessage(
                subject=f'[{settings.PHOENIX_NAME} PHOENIX Reminder] {settings.SEND_MAIL_DEADLINE_REMINDER_HOURS - settings.SEND_MAIL_DEADLINE_MARGIN_HOURS} hours until automatic BRSF submission for session',
                body=f'Hello, in one hour the track and trace data for the session on {send_reminder_session.time.date()} at {send_reminder_session.place} will be submitted to BRSF.\r\nPlease ensure the attendance details for this session are up-to-date.',
                from_email=settings.EMAIL_FROM_ADDRESS,
                to=settings.EMAIL_REMINDER_TO_ADDRESSES
            )
            email.send()
            send_reminder_session.warning_sent = True
            send_reminder_session.save()


def send_pdfs():
    from phoenix.models import Session
    from phoenix.print_views import make_pdf

    max_age_hours = settings.SEND_MAIL_DEADLINE_HOURS - settings.SEND_MAIL_DEADLINE_MARGIN_HOURS
    now = timezone.now()

    older_than = now - timedelta(hours=max_age_hours)
    sessions = Session.objects.filter(stats_uploaded=False, time__lte=older_than)
    if sessions:
        for session in sessions:
            with tempfile.TemporaryFile(suffix='.pdf') as pdfstream:
                make_pdf(pdfstream, session)
                doc_title = f'{settings.PHOENIX_NAME} - {session.time} at {session.place}.pdf'

                if settings.SEND_PDF_MAIL:
                    email = EmailMessage(
                        subject=f'[{settings.PHOENIX_NAME} Tracing Submission] {session.time.date()} session at {session.place}',
                        body=f'This is the automatic track and tracing submission for the {settings.PHOENIX_NAME} session at {session.place}, {session.time.ctime()}.',
                        from_email=settings.EMAIL_FROM_ADDRESS,
                        to=settings.EMAIL_PDF_TO_ADDRESSES
                    )

                    pdfstream.seek(0)
                    email.attach(filename=doc_title, content=pdfstream.read(), mimetype='application/pdf')
                    email.send()

                if settings.SEND_TO_DROPBOX:
                    pdfstream.seek(0)
                    dbx = dropbox.Dropbox(settings.DROPBOX_AUTH_TOKEN)
                    dbx.files_upload(pdfstream.read(), path=f'/{settings.PHOENIX_NAME}/{doc_title}')

                session.stats_uploaded = True
                session.save()


@app.task(ignore_result=True)
def mail_pdf_task():
    # Alright, we'll go through the list of sessions
    # which have not yet had their results sent.
    # the deadline is 24 hours after an event
    # so, any event 23 hours old will have their forms sent.

    if settings.SEND_REMINDER_MAIL:
        send_reminders()

    if settings.SEND_PDF_MAIL or settings.SEND_TO_DROPBOX:
        send_pdfs()


app.add_periodic_task(
    60,
    mail_pdf_task.s()
)
