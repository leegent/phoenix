from django.db import models


class Bubble(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Member(models.Model):
    legal_name = models.CharField(max_length=200)
    locale = models.CharField(max_length=200)
    contact_number = models.CharField(max_length=100)
    bubble = models.ForeignKey('Bubble', on_delete=models.CASCADE)

    class Meta:
        ordering = ['legal_name']

    def __str__(self):
        return self.legal_name


class Session(models.Model):
    time = models.DateTimeField()
    place = models.CharField(max_length=200)
    bubble = models.ForeignKey('Bubble', on_delete=models.CASCADE)
    attendees = models.ManyToManyField('Member', through='SessionAttendance')
    warning_sent = models.BooleanField(default=False)
    stats_uploaded = models.BooleanField(default=False)
    last_modified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-time']

    def __str__(self):
        return f'Bubble {self.bubble} session on {self.time.ctime()} at {self.place}'


class SessionAttendance(models.Model):
    member = models.ForeignKey('Member', on_delete=models.CASCADE)
    session = models.ForeignKey('Session', on_delete=models.CASCADE)
    temperature = models.FloatField(null=True)
    test_pass = models.BooleanField(default=False)

