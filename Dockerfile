FROM python:3.8-slim

ENV PYTHONBUFFERED=1 \
    PORT=8000 \
    DEBUG="False" \
    SECRET_KEY="9rr4cp&pn=d_^c(u-k1p#xs%w2t8lq8k=sas&+c(q_00i!dnx3" \
    ALLOWED_HOSTS="phoenix.defaultcity.skate test.defaultcity.skate" \
    BROKER_URL="redis://redis:6379" \
    PHOENIX_NAME="Default City Roller Derby" \
    DATABASE_URL="postgres://postgres:PASSWORD@db/postgres" \
    SEND_MAIL_DEADLINE_HOURS=24 \
    SEND_MAIL_DEADLINE_MARGIN_HOURS=1 \
    SEND_MAIL_DEADLINE_REMINDER_HOURS=2 \
    EMAIL_HOST="xxx" \
    EMAIL_PORT=465 \
    EMAIL_USE_SSL="True" \
    EMAIL_HOST_USER="mail@defaultcity.skate" \
    EMAIL_HOST_PASSWORD="xxx" \
    EMAIL_FROM_ADDRESS="Default City RD <mail@defaultcity.skate>" \
    EMAIL_PDF_TO_ADDRESSES="mail@tracking.org mail@defaultcity.skate" \
    EMAIL_REMINDER_TO_ADDRESSES="mail@defaultcity.skate lee@defaultcity.skate" \
    DROPBOX_AUTH_TOKEN="xxx" \
    SEND_REMINDER_MAIL="False" \
    SEND_PDF_MAIL="False" \
    SEND_TO_DROPBOX="False"

WORKDIR /app
ADD requirements.txt .
RUN pip install -r requirements.txt
RUN pip install django[argon2]
ADD . .
RUN python manage.py collectstatic --noinput

CMD ["sh", "-c", "gunicorn -t 300 -b 0.0.0.0:$PORT -k gevent harlotsphoenix.wsgi"]
EXPOSE ${PORT}
